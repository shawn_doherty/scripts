#! /bin/bash

#provide a valid kernel config file as an argument
input="$1"

mkdir -p ./configs

# read file for values not set
while IFS= read -r line
do 
    
    if [[ $line =~ "is not set" ]]; then
		mkdir -p ./configs/not-set               
        option_n=$(echo $line | awk '{print $2}')
        echo "$line" >> ./configs/not-set/$option_n
    fi

	if [[ $line =~ "=y" ]]; then
		mkdir -p ./configs/included
		option_y=$(echo "${line::-2}")
		echo "$line" >> ./configs/included/$option_y
	fi

	if [[ $line =~ "=m" ]]; then
		mkdir -p ./configs/modules
		option_m=$(echo "${line::-2}")
		echo "$line" >> ./configs/modules/$option_m
	fi

done < "$input"

echo "individual config files have been placed in the configs directory"
