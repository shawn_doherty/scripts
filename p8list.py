#!/usr/bin/python
PATH='/usr/share/Modules/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/test/.local/bin:/home/test/bin'
#
# Shawn Doherty
# 3/15/19
# 
# Beaker TEIID connect
# Generate report of P9 systems
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import psycopg2
import datetime
import getpass
from pprint import pprint
from datetime import datetime
import smtplib
import os

apppw=os.environ['APP_SPECIFIC_PASSWORD']
teiid_login=os.environ['TEIID_LOGIN']
teiid_password=os.environ['TEIID_PASSWORD']

def get_data():
    conn = psycopg2.connect("dbname=public sslmode=require user=%s password=%s host=virtualdb.engineering.redhat.com port=5432" % (teiid_login,teiid_password))
    conn.set_isolation_level(0)
    cursor = conn.cursor()
    cursor.execute("""
            SELECT system.fqdn, system.status, system.status_reason, tg_user.user_name
            FROM beaker.system 
            LEFT JOIN beaker.cpu ON (system.id = cpu.system_id)
            LEFT JOIN beaker.tg_user ON (tg_user.user_id = system.user_id)
            WHERE cpu.model_name LIKE '%POWER8%'
            ORDER BY system.status,system.fqdn 
            """ 
        )

    data = cursor.fetchall()
    conn.close()
    return data

def send_mail():
    now=datetime.now()
    email_addr='sdoherty@redhat.com'
    recipients=['sdoherty@redhat.com','rihogan@redhat.com','bdonahue@redhat.com', 'peterm@redhat.com','jnicolet@redhat.com','abitaraf@redhat.com','mahall@redhat.com','jamgibso@redhat.com','dbaird@redhat.com']
    subject = 'p8 report for '+ now.strftime('%A, %d %b %Y %l:%M %p')
    f=open("p8report.txt","r")
    if f.mode == "r":
        contents = f.read()
        body = contents

    smtpObj = smtplib.SMTP('smtp.gmail.com', 587)
    smtpObj.ehlo()
    smtpObj.starttls()
    smtpObj.login('sdoherty@redhat.com', apppw)

    smtpObj.sendmail(email_addr, recipients,'To: %s.\nSubject: %s.\n\n%s\n\nThanks, Shawn'% (email_addr,subject,body))

def generate_report():
    sys_data=[]
    data = get_data()
    f=open("p8report.txt", "w+")
    for row in data:
        sys_data.append({'fqdn': row[0],'status': row[1],'reason':row[2],'username':row[3]})

    broken_systems=[]
    automated_systems=[]
    resource_systems=[]
    for x in sys_data:
        if(x['status']) == 'Broken':
            broken_systems.append(x)
        elif (x['status']) == 'Automated':
            automated_systems.append(x)
        elif (x['status']) == 'Manual':
            resource_systems.append(x)

    DIVIDER="---------------------------\n"
    now=datetime.now()
    f.write(now.strftime('%A, %d %b %Y %l:%M %p\n'))
    f.write ( str(len(broken_systems))+ " broken of "+ str(len(broken_systems)+len(automated_systems)+len(resource_systems)) + " systems in beaker\n")
    f.write(DIVIDER)
    f.write("Broken Systems [" + str(len(broken_systems)) +"]\n")
    f.write(DIVIDER)
    for x in broken_systems:
        if x['reason'] is not None:
            f.write(x['fqdn'] + " : "+ x['reason']+"\n")
        else:
            f.write(x['fqdn']+"\n")
    f.write(DIVIDER)
    f.write("Automated Systems [" + str(len(automated_systems)) +"]\n")
    f.write(DIVIDER)
    for x in automated_systems:
        if x['username'] is not None:
            f.write(x['fqdn'] + "   User:" + x['username']+"\n")
        else:
            f.write(x['fqdn']+"\n")

    f.write(DIVIDER)
    f.write("Resources [" + str(len(resource_systems)) +"]\n")
    f.write(DIVIDER)
    for x in resource_systems:
        if x['username'] is not None:
            f.write(x['fqdn'] + "   User:" + x['username']+"\n")
        else:
            f.write(x['fqdn']+"\n")

    f.close()

generate_report()
send_mail()
